import yfinance as yf
import datetime

#Stock Symbols
symbols = ['AAPL', 'TSLA', 'GOOGL']

# Set time frame(2018-2023)
end_date = datetime.datetime.now().strftime('%Y-%m-%d')
start_date = (datetime.datetime.now() - datetime.timedelta(days=5*365)).strftime('%Y-%m-%d')

#Downloading data and saving them into separate csv files
for symbol in symbols:
    data = yf.download(symbol, start=start_date, end=end_date)
    filename = f"{symbol}_Stock_Data.csv"
    data.to_csv(filename)
    
    print(f"Data for {symbol} are saved in {filename}.")