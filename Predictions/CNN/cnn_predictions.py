from keras.models import load_model
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Conv1D, MaxPooling1D, Flatten, Dense
import matplotlib.pyplot as plt

# Load data from CSV file
google_data = pd.read_csv(r"C:\Users\dante\OneDrive\Desktop\StockMarket\Data\GOOGL_Stock_Data.csv")
apple_data = pd.read_csv(r"C:\Users\dante\OneDrive\Desktop\StockMarket\Data\AAPL_Stock_Data.csv")
tesla_data = pd.read_csv(r"C:\Users\dante\OneDrive\Desktop\StockMarket\Data\TSLA_Stock_Data.csv")

# Load the trained model
google_model = load_model(r"C:\Users\dante\OneDrive\Desktop\StockMarket\ML_models\google_cnn_model.h5")
apple_model = load_model(r"C:\Users\dante\OneDrive\Desktop\StockMarket\ML_models\apple_cnn_model.h5")
tesla_model = load_model(r"C:\Users\dante\OneDrive\Desktop\StockMarket\ML_models\tesla_cnn_model.h5")

# Select only the necessary attribute (Close price)
google_data = google_data[['Date', 'Close']]
apple_data = apple_data[['Date', 'Close']]
tesla_data = tesla_data[['Date', 'Close']]

# Convert Date column to datetime type
google_data['Date'] = pd.to_datetime(google_data['Date'])
apple_data['Date'] = pd.to_datetime(apple_data['Date'])
tesla_data['Date'] = pd.to_datetime(tesla_data['Date'])

# Filter data for the year 2023
start_date_2023 = pd.to_datetime('2023-01-01')
end_date_2023 = pd.to_datetime('2023-12-31')
google_data_2023 = google_data.loc[(google_data['Date'] >= start_date_2023) & (google_data['Date'] <= end_date_2023)]
apple_data_2023 = apple_data.loc[(apple_data['Date'] >= start_date_2023) & (apple_data['Date'] <= end_date_2023)]
tesla_data_2023 = tesla_data.loc[(tesla_data['Date'] >= start_date_2023) & (tesla_data['Date'] <= end_date_2023)]

# Set the index to Date
google_data_2023.set_index('Date', inplace=True)
apple_data_2023.set_index('Date', inplace=True)
tesla_data_2023.set_index('Date', inplace=True)

# Data normalization
scaler = MinMaxScaler(feature_range=(0, 1))
google_scaled_data = scaler.fit_transform(google_data_2023.values)
apple_scaled_data = scaler.fit_transform(apple_data_2023.values)
tesla_scaled_data = scaler.fit_transform(tesla_data_2023.values)

# Set hyperparameters
window_size = 60

# Function to create input and output sequences
def create_sequences(dataset, window_size):
    X, Y = [], []
    for i in range(len(dataset) - window_size):
        X.append(dataset[i:(i + window_size)])
        Y.append(dataset[i + window_size])
    return np.array(X), np.array(Y)

# Create sequences for 2023 data
GX_2023, GY_2023 = create_sequences(google_scaled_data, window_size)
AX_2023, AY_2023 = create_sequences(apple_scaled_data, window_size)
TX_2023, TY_2023 = create_sequences(tesla_scaled_data, window_size)

# Predict on the 2023 data
google_predictions_2023 = google_model.predict(GX_2023)
apple_predictions_2023 = apple_model.predict(AX_2023)
tesla_predictions_2023 = tesla_model.predict(TX_2023)

# Inverse transform the predictions and actual values
google_predictions_2023 = scaler.inverse_transform(google_predictions_2023)
GY_2023 = scaler.inverse_transform(GY_2023)
apple_predictions_2023 = scaler.inverse_transform(apple_predictions_2023)
AY_2023 = scaler.inverse_transform(AY_2023)
tesla_predictions_2023 = scaler.inverse_transform(tesla_predictions_2023)
TY_2023 = scaler.inverse_transform(TY_2023)

# Get the dates for the 2023 data
dates_2023 = google_data_2023.index[window_size:]
dates_2023 = apple_data_2023.index[window_size:]
dates_2023 = tesla_data_2023.index[window_size:]

# Plot the actual and predicted values
plt.plot(dates_2023, AY_2023, label='Actual')
plt.plot(dates_2023, apple_predictions_2023, label='Predicted')
plt.xlabel('Date')
plt.ylabel('Close Price')
plt.title('Actual vs. Predicted Apple Stock Prices for 2023 with CNN')
plt.legend()
plt.xticks(rotation=45)
plt.show()

plt.plot(dates_2023, GY_2023, label='Actual')
plt.plot(dates_2023, google_predictions_2023, label='Predicted')
plt.xlabel('Date')
plt.ylabel('Close Price')
plt.title('Actual vs. Predicted Google Stock Prices for 2023 with CNN')
plt.legend()
plt.xticks(rotation=45)
plt.show()

plt.plot(dates_2023, TY_2023, label='Actual')
plt.plot(dates_2023, tesla_predictions_2023, label='Predicted')
plt.xlabel('Date')
plt.ylabel('Close Price')
plt.title('Actual vs. Predicted Tesla Stock Prices for 2023 with CNN')
plt.legend()
plt.xticks(rotation=45)
plt.show()

# Create DataFrames for predictions and actual values
predictions_2023_df = pd.DataFrame(google_predictions_2023, columns=['Predicted Close'])
actual_values_df = pd.DataFrame(GY_2023, columns=['Actual Close'])
predictions_2023_df = pd.DataFrame(apple_predictions_2023, columns=['Predicted Close'])
actual_values_df = pd.DataFrame(AY_2023, columns=['Actual Close'])
predictions_2023_df = pd.DataFrame(tesla_predictions_2023, columns=['Predicted Close'])
actual_values_df = pd.DataFrame(TY_2023, columns=['Actual Close'])

# Save the predictions to a separate CSV file
predictions_2023_df.to_csv("cnn_google_predictions_2023.csv", index=False)
predictions_2023_df.to_csv("cnn_apple_predictions_2023.csv", index=False)
predictions_2023_df.to_csv("cnn_tesla_predictions_2023.csv", index=False)

# Save the actual values to a separate CSV file
actual_values_df.to_csv("cnn_google_actual_values_2023.csv", index=False)
actual_values_df.to_csv("cnn_apple_actual_values_2023.csv", index=False)
actual_values_df.to_csv("cnn_tesla_actual_values_2023.csv", index=False)

from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

# Calculate evaluation metrics for Google stock predictions
google_mse = mean_squared_error(GY_2023, google_predictions_2023)
google_mae = mean_absolute_error(GY_2023, google_predictions_2023)
google_r2 = r2_score(GY_2023, google_predictions_2023)

# Calculate evaluation metrics for Apple stock predictions
apple_mse = mean_squared_error(AY_2023, apple_predictions_2023)
apple_mae = mean_absolute_error(AY_2023, apple_predictions_2023)
apple_r2 = r2_score(AY_2023, apple_predictions_2023)

# Calculate evaluation metrics for Tesla stock predictions
tesla_mse = mean_squared_error(TY_2023, tesla_predictions_2023)
tesla_mae = mean_absolute_error(TY_2023, tesla_predictions_2023)
tesla_r2 = r2_score(TY_2023, tesla_predictions_2023)

# Create a bar plot for evaluation metrics
metrics = ['MSE', 'MAE', 'R-squared']
google_metrics = [google_mse, google_mae, google_r2]
apple_metrics = [apple_mse, apple_mae, apple_r2]
tesla_metrics = [tesla_mse, tesla_mae, tesla_r2]

x = np.arange(len(metrics))
width = 0.25

fig, ax = plt.subplots()
rects1 = ax.bar(x - width, google_metrics, width, label='Google')
rects2 = ax.bar(x, apple_metrics, width, label='Apple')
rects3 = ax.bar(x + width, tesla_metrics, width, label='Tesla')

ax.set_ylabel('Value')
ax.set_title('Evaluation Metrics for Stock Predictions')
ax.set_xticks(x)
ax.set_xticklabels(metrics)
ax.legend()

# Attach a label above each bar displaying its value
def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.annotate(f'{height:.2f}', xy=(rect.get_x() + rect.get_width() / 2, height), xytext=(0, 3),
                    textcoords="offset points", ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)

plt.show()
