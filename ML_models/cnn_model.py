import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Conv1D, MaxPooling1D, Flatten, Dense

# Load data from CSV file
data = pd.read_csv(r"C:\Users\dante\OneDrive\Desktop\StockMarket\Data\GOOGL_Stock_Data.csv")

# Select only the necessary attribute (Close price)
data = data[['Date', 'Close']]

# Convert Date column to datetime type
data['Date'] = pd.to_datetime(data['Date'])

# Filter data from 2018 to 2022
start_date = pd.to_datetime('2018-01-01')
end_date = pd.to_datetime('2022-12-31')
filtered_data = data.loc[(data['Date'] >= start_date) & (data['Date'] <= end_date)]

# Set the index to Date
filtered_data.set_index('Date', inplace=True)

# Data normalization
scaler = MinMaxScaler(feature_range=(0, 1))
scaled_data = scaler.fit_transform(filtered_data.values)

# Set hyperparameters
window_size = 60

# Function to create input and output sequences
def create_sequences(dataset, window_size):
    X, Y = [], []
    for i in range(len(dataset) - window_size):
        X.append(dataset[i:(i + window_size)])
        Y.append(dataset[i + window_size])
    return np.array(X), np.array(Y)

# Create input and output sequences
X, Y = create_sequences(scaled_data, window_size)

# Split the data into training and testing sets
train_size = int(0.8 * len(X))
X_train, X_test = X[:train_size], X[train_size:]
Y_train, Y_test = Y[:train_size], Y[train_size:]

# Build the CNN model
model = Sequential()
model.add(Conv1D(filters=32, kernel_size=3, activation='relu', input_shape=(window_size, 1)))
model.add(MaxPooling1D(pool_size=2))
model.add(Flatten())
model.add(Dense(64, activation='relu'))
model.add(Dense(1))

# Compile the model
model.compile(optimizer='adam', loss='mean_squared_error')

# Train the model
model.fit(X_train, Y_train, epochs=100, batch_size=32)

# Save the trained model
model.save("google_cnn_model.h5")