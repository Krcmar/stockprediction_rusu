import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from tensorflow import keras
from keras.models import Model
from keras.models import Sequential
from keras.layers import GRU
from keras.layers import Dense

# Load the data
data = pd.read_csv(r"C:\Users\dante\OneDrive\Desktop\StockMarket\Data\GOOGL_Stock_Data.csv")

# Filter the data for the years 2018 to 2022
start_date = "2018-01-01"
end_date = "2022-12-31"
data = data[(data["Date"] >= start_date) & (data["Date"] <= end_date)]

# Preprocess the data
scaler = MinMaxScaler(feature_range=(0, 1))
scaled_data = scaler.fit_transform(data["Close"].values.reshape(-1, 1))

# Split the data into training and testing sets
train_size = int(len(scaled_data) * 0.8)
train_data = scaled_data[:train_size]
test_data = scaled_data[train_size:]

# Define the window size for input sequences
window_size = 60

# Create input sequences
X_train = []
y_train = []
for i in range(window_size, len(train_data)):
    X_train.append(train_data[i - window_size:i, 0])
    y_train.append(train_data[i, 0])
X_train, y_train = np.array(X_train), np.array(y_train)

# Reshape the input sequences for GRU model (add an extra dimension)
X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))

# Build the GRU model
model = Sequential()
model.add(GRU(units=50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
model.add(GRU(units=50))
model.add(Dense(units=1))

# Compile the model
model.compile(optimizer='adam', loss='mean_squared_error')

# Train the model
model.fit(X_train, y_train, epochs=100, batch_size=32)

# Save the trained model
model.save("google_gru_model.h5")