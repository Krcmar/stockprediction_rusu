import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from tensorflow import keras
from keras.models import Model
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense

# Load data from CSV file
data = pd.read_csv(r"C:\Users\dante\OneDrive\Desktop\StockMarket\Data\AAPL_Stock_Data.csv")

# Filter the data for the years 2018 to 2022
start_date = "2018-01-01"
end_date = "2022-12-31"
data = data[(data["Date"] >= start_date) & (data["Date"] <= end_date)]

# Select only the necessary attribute (Close price)
data = data[['Close']]

# Data normalization
scaler = MinMaxScaler(feature_range=(0, 1))
scaled_data = scaler.fit_transform(data.values)

# Split data into training and testing sets
train_data = scaled_data[:int(0.8 * len(data))]
test_data = scaled_data[int(0.8 * len(data)):]

# Function to create training and testing datasets
def create_dataset(dataset, time_steps=1):
    X, Y = [], []
    for i in range(len(dataset) - time_steps - 1):
        a = dataset[i:(i + time_steps), 0]
        X.append(a)
        Y.append(dataset[i + time_steps, 0])
    return np.array(X), np.array(Y)

# Set hyperparameters
time_steps = 30
batch_size = 32
epochs = 100

# Create training and testing datasets
X_train, Y_train = create_dataset(train_data, time_steps)
X_test, Y_test = create_dataset(test_data, time_steps)

# Reshape input data into the shape expected by LSTM (samples, time steps, features)
X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

# Build the LSTM model
model = Sequential()
model.add(LSTM(units=50, return_sequences=True, input_shape=(time_steps, 1)))
model.add(LSTM(units=50))
model.add(Dense(units=1))

# Compile the model
model.compile(optimizer='adam', loss='mean_squared_error')

# Train the model
model.fit(X_train, Y_train, batch_size=batch_size, epochs=epochs)

# Save the trained model
model.save("apple_lstm_model.h5")
